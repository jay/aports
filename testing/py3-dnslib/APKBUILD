# Contributor: Fabian Affolter <fabian@affolter-engineering.ch>
# Maintainer: Fabian Affolter <fabian@affolter-engineering.ch>
pkgname=py3-dnslib
_pkgname=dnslib
pkgver=0.9.21
pkgrel=0
pkgdesc="simple library to encode/decode DNS wire-format packets"
url="https://github.com/paulc/dnslib"
arch="noarch"
license="BSD-2-Clause"
depends="python3"
makedepends="py3-setuptools"
source="https://files.pythonhosted.org/packages/source/${_pkgname:0:1}/$_pkgname/$_pkgname-$pkgver.tar.gz"
builddir="$srcdir"/$_pkgname-$pkgver

replaces=py-dnslib # Backwards compatibility
provides=py-dnslib=$pkgver-r$pkgrel # Backwards compatibility

# secfixes:
#   0.9.19-r0:
#     - CVE-2022-22846

build() {
	python3 setup.py build
}

check() {
	PYTHONPATH="$PWD/build/lib" python3 dnslib/test_decode.py
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

sha512sums="
0822f1fac78a8b246298a8d143d080bc652da6b550193902caf90ab33118a2d6cb59e7450760462af17adcd6dffc55b6b2cb0d7bd64b1b607b1f5790c05fbfaa  dnslib-0.9.21.tar.gz
"
