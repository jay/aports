# Contributor: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
# Maintainer: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
pkgname=py3-xarray
_pkgorig=xarray
pkgver=2022.6.0
pkgrel=1
pkgdesc="N-D labeled arrays and datasets in Python"
url="https://xarray.dev"
arch="noarch !s390x" # assertionErrors
license="Apache-2.0"
depends="python3 py3-numpy py3-packaging py3-pandas"
makedepends="python3-dev py3-setuptools_scm"
checkdepends="py3-coverage py3-mock py3-pytest py3-pytest-cov"
source="$pkgname-$pkgver.tar.gz::https://files.pythonhosted.org/packages/source/x/xarray/xarray-$pkgver.tar.gz"
builddir="$srcdir/$_pkgorig-$pkgver"

build() {
	python3 setup.py build
}

check() {
	python3 -m pytest -k 'not test_dataset and not test_distributed and not test_dataarray'
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"

	rm -r "$pkgdir"/usr/lib/python3.*/site-packages/xarray/tests
}

sha512sums="
415bd84246d6d2b8aebf316ac9ef82ca7afeacce718349b75479c0aa693e2866dfe236e6dcaf41cae68afd7a99408f5027c938e6b7f447f5323e076b804f5165  py3-xarray-2022.6.0.tar.gz
"
